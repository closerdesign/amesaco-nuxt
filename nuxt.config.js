export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Andres Mesa Co - Agencia Digital',
    htmlAttrs: {
      lang: 'es',
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: 'format-detection', content: 'telephone=no'},
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'stylesheet', type: 'text/css', href: '/css/bootstrap.min.css'},
      {rel: 'stylesheet', type: 'text/css', href: '/css/font-awesome.min.css'},
      {rel: 'stylesheet', type: 'text/css', href: '/css/slick.css'},
      {rel: 'stylesheet', type: 'text/css', href: '/css/slick-theme.css'},
      {rel: 'stylesheet', type: 'text/css', href: '/css/jquery.fancybox.css'},
      {rel: 'stylesheet', type: 'text/css', href: '/bower_components/sweetalert/dist/sweetalert.css'},
      {rel: 'stylesheet', type: 'text/css', href: '/css/style.css'},
    ],
    script: [
      {type: 'text/javascript', src: '/js/jquery-1.11.3.min.js'},
      {type: 'text/javascript', src: '/js/jquery-migrate-1.2.1.min.js'},
      {type: 'text/javascript', src: '/js/bootstrap.min.js'},
      {type: 'text/javascript', src: '/js/jquery.easing.min.js'},
      {type: 'text/javascript', src: '/js/smoothscroll.js'},
      {type: 'text/javascript', src: '/js/response.min.js'},
      {type: 'text/javascript', src: '/js/jquery.placeholder.min.js'},
      {type: 'text/javascript', src: '/js/jquery.fitvids.js'},
      {type: 'text/javascript', src: '/js/jquery.imgpreload.min.js'},
      {type: 'text/javascript', src: '/js/waypoints.min.js'},
      {type: 'text/javascript', src: '/js/slick.min.js'},
      {type: 'text/javascript', src: '/js/jquery.fancybox.pack.js'},
      {type: 'text/javascript', src: '/js/jquery.fancybox-media.js'},
      {type: 'text/javascript', src: '/js/jquery.counterup.min.js'},
      {type: 'text/javascript', src: '/js/parallax.min.js'},
      {
        type: 'text/javascript',
        src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDE7s3AMgMt8XkGB3xZemuH4MU_HKNSpFw'
      },
      {type: 'text/javascript', src: '/js/gmaps.js'},
      {type: 'text/javascript', src: '/bower_components/sweetalert/dist/sweetalert.min.js'},
      {type: 'text/javascript', src: '/js/script.js'},
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios'
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
